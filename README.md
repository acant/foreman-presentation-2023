# Foreman Presentation 2023

```
foreman start
```

```
foreman start -f Procfile.dev
```

## Rails examples

`bin/dev`

```
#!/usr/bin/env sh

if ! gem list foreman -i --silent; then
  echo "Installing foreman..."
  gem install foreman
fi

exec foreman start -f Procfile.dev "$@"
```

`Procfile.dev`

```
web: unset PORT && bin/rails server
js: yarn build ; yarn build --watch
css: yarn build:css ; yarn build:css --watch
```

## External Links
* http://blog.daviddollar.org/2011/05/06/introducing-foreman.html
* https://github.com/ddollar/foreman
* https://xkcd.com/1205/
* https://devcenter.heroku.com/articles/procfile
* https://judoscale.com/guides/six-tips-for-mastering-your-procfile
* https://theforeman.org/
